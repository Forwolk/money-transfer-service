#Money Transfer Service (MTS)

Test task from **Revolut**

Design and implement RESTfull API for money transfers between accounts

###Requirements
1. Keep it simple and to the point
2. Use whatever frameworks except Spring
3. In-memory datasource
4. Result should be standalone
5. Demonstrate with tests