package ru.forwolk.test.mts.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import static ru.forwolk.test.mts.common.ServiceConstants.DEFAULT_ACCOUNT_NAME;

@Data
@Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name = DEFAULT_ACCOUNT_NAME;
    private int balance = 0;
}
