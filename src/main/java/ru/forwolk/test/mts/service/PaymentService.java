package ru.forwolk.test.mts.service;

import ru.forwolk.test.mts.entity.Account;

public interface PaymentService {
    void deposit(Account account, int amount);
    void withdraw(Account account, int amount);
    void transfer(Account accountFrom, Account accountTo, int amount);
}
