package ru.forwolk.test.mts.service;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class SingletonManager {
    private static volatile SingletonManager instance;
    private final Map<Class, Object> binds = new HashMap<>();

    private SingletonManager(){}

    public static SingletonManager getInstance() {
        if (instance == null) {
            synchronized (SingletonManager.class) {
                if (instance == null) {
                    instance = new SingletonManager();
                }
            }
        }
        return instance;
    }

    public <T> void bind(Class<? extends T> c, T object) {
        synchronized (binds) {
            log.info("Bind {} to {}", c.getSimpleName(), object.toString());
            binds.put(c, object);
        }
    }

    public <T> T get(Class<? extends T> c) {
        Object o;
        synchronized (binds) {
            o = binds.get(c);
        }
        if (o != null) {
            return (T) binds.get(c);
        } else {
            return null;
        }
    }
}
