package ru.forwolk.test.mts.service.impl;

import lombok.extern.slf4j.Slf4j;
import ru.forwolk.test.mts.data.DataSource;
import ru.forwolk.test.mts.entity.Account;
import ru.forwolk.test.mts.service.PaymentService;
import ru.forwolk.test.mts.service.SingletonManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.util.concurrent.locks.Lock;

@Slf4j
public class PaymentServiceImpl implements PaymentService {
    private EntityManagerFactory entityManagerFactory = SingletonManager.getInstance().get(EntityManagerFactory.class);
    private Lock lock = SingletonManager.getInstance().get(DataSource.class).getDatabaseLock();

    @Override
    public void deposit(Account account, int amount) {
        checkAccountExists(account);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            lock.lock();
            EntityTransaction tx = entityManager.getTransaction();
            tx.begin();
            account = entityManager.find(Account.class, account.getId());
            account.setBalance(account.getBalance() + amount);
            entityManager.merge(account);
            tx.commit();
        } finally {
            lock.unlock();
        }
        log.info("Deposit on account {}. Amount: {}", account, amount);
    }

    @Override
    public void withdraw(Account account, int amount) {
        checkAccountExists(account);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            lock.lock();
            EntityTransaction tx = entityManager.getTransaction();
            tx.begin();
            account = entityManager.find(Account.class, account.getId());
            if (account.getBalance() < amount) {
                log.warn("No enough money: {}", account);
                tx.rollback();
                entityManager.close();
                throw new IllegalStateException("There are not enough money on account");
            }
            account.setBalance(account.getBalance() - amount);
            entityManager.merge(account);
            tx.commit();
        } finally {
            lock.unlock();
        }
        log.info("Withdraw from account {}. Amount: {}", account, amount);
    }

    @Override
    public void transfer(Account accountFrom, Account accountTo, int amount) {
        checkAccountExists(accountFrom);
        checkAccountExists(accountTo);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            lock.lock();
            EntityTransaction tx = entityManager.getTransaction();
            tx.begin();
            accountFrom = entityManager.find(Account.class, accountFrom.getId());
            accountTo = entityManager.find(Account.class, accountTo.getId());
            if (accountFrom.getBalance() < amount) {
                log.warn("No enough money: {}", accountFrom);
                tx.rollback();
                throw new IllegalStateException("There are not enough money on account");
            }
            accountFrom.setBalance(accountFrom.getBalance() - amount);
            accountTo.setBalance(accountTo.getBalance() + amount);
            entityManager.merge(accountFrom);
            entityManager.merge(accountTo);
            tx.commit();
        } finally {
            lock.unlock();
        }
        log.info("Money transfer: {} --> {}. Amount: {}", accountFrom, accountTo, amount);
    }

    private void checkAccountExists(Account account) {
        if (account.getId() == null) {
            log.warn("Account not exists: {}", account);
            throw new IllegalStateException("Account has not saved yet");
        }
    }
}
