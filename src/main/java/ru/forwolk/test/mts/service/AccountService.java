package ru.forwolk.test.mts.service;

import ru.forwolk.test.mts.entity.Account;

import java.util.Optional;

public interface AccountService {
    Account create(String name, int balance);
    Optional<Account> getAccountById(int id);
    Account update(Account account);
    void delete(Account account);
}
