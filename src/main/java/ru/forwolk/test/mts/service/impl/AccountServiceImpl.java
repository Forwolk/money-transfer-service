package ru.forwolk.test.mts.service.impl;

import lombok.extern.slf4j.Slf4j;
import ru.forwolk.test.mts.data.DataSource;
import ru.forwolk.test.mts.entity.Account;
import ru.forwolk.test.mts.service.AccountService;
import ru.forwolk.test.mts.service.SingletonManager;

import java.util.Optional;

@Slf4j
public class AccountServiceImpl implements AccountService {
    private DataSource dataSource = SingletonManager.getInstance().get(DataSource.class);

    @Override
    public Account create(String name, int balance) {
        Account account = new Account();
        account.setName(name);
        account.setBalance(balance);
        dataSource.save(account);
        log.info("Account created: {}", account);
        return account;
    }

    @Override
    public Optional<Account> getAccountById(int id) {
        return dataSource.findOne(id);
    }

    @Override
    public Account update(Account account) {
        if (account.getId() != null) {
            if (dataSource.findOne(account.getId()).isPresent()) {
                dataSource.save(account);
                log.info("Account updated: {}", account);
            } else {
                return null;
            }
        } else {
            throw new IllegalStateException("Account has not saved yet");
        }
        return account;
    }

    @Override
    public void delete(Account account) {
        dataSource.delete(account);
        log.info("Account deleted: {}", account);
    }
}
