package ru.forwolk.test.mts.data;

import ru.forwolk.test.mts.entity.Account;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.locks.Lock;

public interface DataSource {
    Account save(Account account);
    Optional<Account> findOne(int id);
    Collection<Account> findAll();
    void delete(int id);
    void delete(Account account);
    Lock getDatabaseLock();
}
