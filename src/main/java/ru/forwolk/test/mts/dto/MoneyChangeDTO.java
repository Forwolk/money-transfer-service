package ru.forwolk.test.mts.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MoneyChangeDTO implements Serializable {
    private Integer accountFrom;
    private Integer accountTo;
    private int amount;
    private Operation operation;
}
