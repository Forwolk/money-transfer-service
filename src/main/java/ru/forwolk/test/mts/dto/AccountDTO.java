package ru.forwolk.test.mts.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AccountDTO implements Serializable {
    private Integer id;
    private String name;
    private int balance;
}
