package ru.forwolk.test.mts.dto;

import java.io.Serializable;

public enum  Operation implements Serializable {
    DEPOSIT,
    WITHDRAW,
    TRANSFER
}
