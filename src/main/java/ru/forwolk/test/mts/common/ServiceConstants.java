package ru.forwolk.test.mts.common;

public final class ServiceConstants {
    public static final String DEFAULT_ACCOUNT_NAME = "Unnamed";
    public static final String SUCCESS = "SUCCESS";
    public static final String FIND_ALL_ACCOUNTS_QUERY = "Select a from Account a";
}
