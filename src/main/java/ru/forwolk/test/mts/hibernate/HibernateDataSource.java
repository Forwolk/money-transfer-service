package ru.forwolk.test.mts.hibernate;

import lombok.extern.slf4j.Slf4j;
import ru.forwolk.test.mts.data.DataSource;
import ru.forwolk.test.mts.entity.Account;
import ru.forwolk.test.mts.service.SingletonManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static ru.forwolk.test.mts.common.ServiceConstants.FIND_ALL_ACCOUNTS_QUERY;

@Slf4j
public class HibernateDataSource implements DataSource {
    private EntityManagerFactory entityManagerFactory = SingletonManager.getInstance().get(EntityManagerFactory.class);
    private final Lock lock = new ReentrantLock();

    @Override
    public Account save(Account account) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            lock.lock();
            EntityTransaction tx = entityManager.getTransaction();
            if (isAccountNew(account)) {
                tx.begin();
                entityManager.persist(account);
                tx.commit();
            } else {
                tx.begin();
                entityManager.merge(account);
                tx.commit();
            }
        } finally {
            lock.unlock();
        }
        return account;
    }

    @Override
    public Optional<Account> findOne(int id) {
        EntityManager entityManager;
        entityManager = entityManagerFactory.createEntityManager();
        try {
            lock.lock();
            return Optional.ofNullable(entityManager.find(Account.class, id));
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Collection<Account> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            lock.lock();
            TypedQuery<Account> query = entityManager.createQuery(FIND_ALL_ACCOUNTS_QUERY, Account.class);
            return query.getResultList();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void delete(int id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            lock.lock();
            EntityTransaction tx = entityManager.getTransaction();
            tx.begin();
            Account account = entityManager.find(Account.class, id);
            if (account != null) {
                entityManager.remove(account);
            }
            tx.commit();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void delete(Account account) {
        delete(account.getId());
    }

    @Override
    public Lock getDatabaseLock() {
        return lock;
    }

    private boolean isAccountNew(Account account) {
        return account.getId() == null;
    }
}
