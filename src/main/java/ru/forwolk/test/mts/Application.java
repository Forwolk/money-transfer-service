package ru.forwolk.test.mts;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import ru.forwolk.test.mts.data.DataSource;
import ru.forwolk.test.mts.hibernate.HibernateDataSource;
import ru.forwolk.test.mts.map.AccountMapper;
import ru.forwolk.test.mts.service.AccountService;
import ru.forwolk.test.mts.service.PaymentService;
import ru.forwolk.test.mts.service.SingletonManager;
import ru.forwolk.test.mts.service.impl.AccountServiceImpl;
import ru.forwolk.test.mts.service.impl.PaymentServiceImpl;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Slf4j
public class Application {
    private static final int DEFAULT_PORT = 8080;

    public void start() {
        configure();
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        String serverPortString = System.getProperty("server.port");
        int serverPort;
        try {
            if (serverPortString != null) {
                serverPort = Integer.parseInt(serverPortString);
            } else {
                serverPort = DEFAULT_PORT;
            }
        } catch (NumberFormatException e) {
            serverPort = DEFAULT_PORT;
        }

        Server jettyServer = new Server(serverPort);
        jettyServer.setHandler(context);
        ServletHolder jerseyServlet = context.addServlet(ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);

        jerseyServlet.setInitParameter(
                "jersey.config.server.provider.packages",
                "ru.forwolk.test.mts.web"
        );

        try {
            jettyServer.start();
            String host = jettyServer.getURI().getHost();
            log.info("Application is started");
            log.info("Server [ip: {}] is running on port {}", host, serverPort);
            jettyServer.join();
        } catch (Exception e) {
            log.error("Error occurred while server running", e);
        } finally {
            jettyServer.destroy();
        }
    }

    public static void main(String[] args) {
        Application application = new Application();
        application.start();
    }

    private void configure() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("externalMapping");
        SingletonManager singletonManager = SingletonManager.getInstance();
        singletonManager.bind(EntityManagerFactory.class, entityManagerFactory);
        singletonManager.bind(DataSource.class, new HibernateDataSource());
        singletonManager.bind(AccountMapper.class, new AccountMapper());
        singletonManager.bind(AccountService.class, new AccountServiceImpl());
        singletonManager.bind(PaymentService.class, new PaymentServiceImpl());
    }
}
