package ru.forwolk.test.mts.map;

import ru.forwolk.test.mts.dto.AccountDTO;
import ru.forwolk.test.mts.entity.Account;

public class AccountMapper {

    public Account map(AccountDTO accountDTO) {
        Account account = new Account();
        account.setId(accountDTO.getId());
        account.setBalance(accountDTO.getBalance());
        account.setName(accountDTO.getName());
        return account;
    }

    public AccountDTO map(Account account){
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(account.getId());
        accountDTO.setBalance(account.getBalance());
        accountDTO.setName(account.getName());
        return accountDTO;
    }
}
