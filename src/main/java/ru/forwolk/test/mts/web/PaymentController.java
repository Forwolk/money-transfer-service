package ru.forwolk.test.mts.web;

import ru.forwolk.test.mts.dto.MoneyChangeDTO;
import ru.forwolk.test.mts.dto.Operation;
import ru.forwolk.test.mts.entity.Account;
import ru.forwolk.test.mts.service.AccountService;
import ru.forwolk.test.mts.service.PaymentService;
import ru.forwolk.test.mts.service.SingletonManager;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

import static ru.forwolk.test.mts.common.ServiceConstants.SUCCESS;

@Path("payment")
public class PaymentController {
    private AccountService accountService = SingletonManager.getInstance().get(AccountService.class);
    private PaymentService paymentService = SingletonManager.getInstance().get(PaymentService.class);

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response payment(MoneyChangeDTO moneyChangeDTO) {
        if (moneyChangeDTO == null || moneyChangeDTO.getOperation() == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        Operation operation = moneyChangeDTO.getOperation();
        Optional<Account> accountFromOptional = moneyChangeDTO.getAccountFrom() == null ? Optional.empty() : accountService.getAccountById(moneyChangeDTO.getAccountFrom());
        Optional<Account> accountToOptional = moneyChangeDTO.getAccountTo() == null ? Optional.empty() : accountService.getAccountById(moneyChangeDTO.getAccountTo());
        int amount = moneyChangeDTO.getAmount();
        if (amount < 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        switch (operation) {
            case DEPOSIT:
                if (accountToOptional.isPresent()) {
                    paymentService.deposit(accountToOptional.get(), amount);
                    return Response.ok(SUCCESS).build();
                } else {
                    return Response.status(Response.Status.BAD_REQUEST).build();
                }
            case WITHDRAW:
                if (accountFromOptional.isPresent()) {
                    paymentService.withdraw(accountFromOptional.get(), amount);
                    return Response.ok(SUCCESS).build();
                } else {
                    return Response.status(Response.Status.BAD_REQUEST).build();
                }
            case TRANSFER:
                if (accountFromOptional.isPresent() && accountToOptional.isPresent()) {
                    paymentService.transfer(accountFromOptional.get(), accountToOptional.get(), amount);
                    return Response.ok(SUCCESS).build();
                } else {
                    return Response.status(Response.Status.BAD_REQUEST).build();
                }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
