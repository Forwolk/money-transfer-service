package ru.forwolk.test.mts.web;

import lombok.extern.slf4j.Slf4j;
import ru.forwolk.test.mts.dto.AccountDTO;
import ru.forwolk.test.mts.entity.Account;
import ru.forwolk.test.mts.map.AccountMapper;
import ru.forwolk.test.mts.service.AccountService;
import ru.forwolk.test.mts.service.SingletonManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Slf4j
@Path("account")
public class AccountController {
    private AccountService accountService = SingletonManager.getInstance().get(AccountService.class);
    private AccountMapper accountMapper = SingletonManager.getInstance().get(AccountMapper.class);

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(AccountDTO accountDTO) {
        if (accountDTO == null || accountDTO.getId() != null || accountDTO.getName() == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        Account account = accountService.create(accountDTO.getName(), accountDTO.getBalance());
        accountDTO = accountMapper.map(account);
        return Response.ok(accountDTO).build();
    }

    @GET
    public Response read(@QueryParam("id") int id){
        if (id < 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        Optional<Account> accountOptional = accountService.getAccountById(id);
        if (accountOptional.isPresent()) {
            AccountDTO accountDTO = accountMapper.map(accountOptional.get());
            return Response.ok(accountDTO).build();
        } else {
            log.warn("Account with id '{}' not found", id);
            return Response.status(Response.Status.BAD_REQUEST).entity("Account not found").build();
        }
    }

    @PUT
    public Response update(AccountDTO accountDTO) {
        if (accountDTO == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        try {
            Account account = accountMapper.map(accountDTO);
            account = accountService.update(account);
            if (account == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity("Account not found").build();
            }
            return Response.ok(accountDTO).build();
        } catch (RuntimeException e) {
            log.error("Error occurred while updating account", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @DELETE
    public Response delete(@QueryParam("id") int id) {
        if (id < 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        Optional<Account> accountOptional = accountService.getAccountById(id);
        if (accountOptional.isPresent()) {
            accountService.delete(accountOptional.get());
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity("Account not found").build();
        }
    }
}
