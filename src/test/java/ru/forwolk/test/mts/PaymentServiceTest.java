package ru.forwolk.test.mts;

import org.junit.Test;
import ru.forwolk.test.mts.entity.Account;
import ru.forwolk.test.mts.service.AccountService;
import ru.forwolk.test.mts.service.PaymentService;
import ru.forwolk.test.mts.service.SingletonManager;

import static org.junit.Assert.assertEquals;
import static ru.forwolk.test.mts.TestConstants.TEST_ACCOUNT2_NAME;
import static ru.forwolk.test.mts.TestConstants.TEST_ACCOUNT_NAME;

public class PaymentServiceTest extends AbstractServiceTest {

    @Test
    public void depositTest() {
        AccountService accountService = SingletonManager.getInstance().get(AccountService.class);
        Account account = accountService.create(TEST_ACCOUNT_NAME, 0);
        PaymentService paymentService = SingletonManager.getInstance().get(PaymentService.class);
        int amount = (int) (10000 * Math.random());
        paymentService.deposit(account, amount);
        assertEquals(amount, accountService.getAccountById(account.getId()).get().getBalance());
    }

    @Test
    public void withdrawTest() {
        AccountService accountService = SingletonManager.getInstance().get(AccountService.class);
        int balance = (int) (10000 * (2 + Math.random()));
        Account account = accountService.create(TEST_ACCOUNT_NAME, balance);
        PaymentService paymentService = SingletonManager.getInstance().get(PaymentService.class);
        int amount = (int) (10000 * Math.random());
        paymentService.withdraw(account, amount);
        assertEquals(balance - amount, accountService.getAccountById(account.getId()).get().getBalance());
    }

    @Test(expected = IllegalStateException.class)
    public void noMoneyWithdrawTest() {
        AccountService accountService = SingletonManager.getInstance().get(AccountService.class);
        int balance = (int) (10000 * Math.random());
        Account account = accountService.create(TEST_ACCOUNT_NAME, balance);
        PaymentService paymentService = SingletonManager.getInstance().get(PaymentService.class);
        int amount = (int) (10000 * (2 + Math.random()));
        paymentService.withdraw(account, amount);
    }

    @Test
    public void transferTest() {
        AccountService accountService = SingletonManager.getInstance().get(AccountService.class);
        PaymentService paymentService = SingletonManager.getInstance().get(PaymentService.class);
        int balance1 = 5000;
        int balance2 = 3000;
        int amount = 400;
        Account account1 = accountService.create(TEST_ACCOUNT_NAME, balance1);
        Account account2 = accountService.create(TEST_ACCOUNT2_NAME, balance2);
        paymentService.transfer(account1, account2, amount);
        assertEquals(balance1 - amount, accountService.getAccountById(account1.getId()).get().getBalance());
        assertEquals(balance2 + amount, accountService.getAccountById(account2.getId()).get().getBalance());
    }

    @Test(expected = IllegalStateException.class)
    public void noMoneyTransferTest() {
        AccountService accountService = SingletonManager.getInstance().get(AccountService.class);
        PaymentService paymentService = SingletonManager.getInstance().get(PaymentService.class);
        int balance1 = 5000;
        int balance2 = 3000;
        int amount = 40000;
        Account account1 = accountService.create(TEST_ACCOUNT_NAME, balance1);
        Account account2 = accountService.create(TEST_ACCOUNT2_NAME, balance2);
        paymentService.transfer(account1, account2, amount);
    }
}
