package ru.forwolk.test.mts;

import lombok.SneakyThrows;
import org.junit.BeforeClass;

import java.lang.reflect.Method;

public abstract class AbstractServiceTest {

    @BeforeClass
    @SneakyThrows
    public static void configure() {
        Method method = Application.class.getDeclaredMethod("configure");
        method.setAccessible(true);
        method.invoke(new Application());
    }
}
