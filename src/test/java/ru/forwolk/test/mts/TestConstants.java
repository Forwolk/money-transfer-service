package ru.forwolk.test.mts;

public final class TestConstants {
    static final String TEST_ACCOUNT_NAME = "test-1";
    static final String TEST_ACCOUNT2_NAME = "test-2";
    static final String APPLICATION_URL = "http://localhost:" + System.getProperty("server.port", "8080") + "/";
}
