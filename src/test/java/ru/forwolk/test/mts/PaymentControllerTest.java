package ru.forwolk.test.mts;

import lombok.SneakyThrows;
import org.junit.Test;
import ru.forwolk.test.mts.dto.AccountDTO;
import ru.forwolk.test.mts.dto.MoneyChangeDTO;
import ru.forwolk.test.mts.dto.Operation;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static ru.forwolk.test.mts.TestConstants.APPLICATION_URL;
import static ru.forwolk.test.mts.TestConstants.TEST_ACCOUNT_NAME;

public class PaymentControllerTest extends AbstractControllerTest {

    @Test
    @SneakyThrows
    public void depositTest() {
        int initBalance = 300;
        int amount = 200;
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setName(TEST_ACCOUNT_NAME);
        accountDTO.setBalance(initBalance);
        String response = post(APPLICATION_URL + "account", "POST", gson.toJson(accountDTO));
        AccountDTO responseDTO = gson.fromJson(response, AccountDTO.class);
        MoneyChangeDTO moneyChangeDTO = new MoneyChangeDTO();
        moneyChangeDTO.setAccountTo(responseDTO.getId());
        moneyChangeDTO.setAmount(amount);
        moneyChangeDTO.setOperation(Operation.DEPOSIT);
        post(APPLICATION_URL + "payment", "POST", gson.toJson(moneyChangeDTO));
        response = get(APPLICATION_URL + "account?id=" + responseDTO.getId());
        accountDTO = gson.fromJson(response, AccountDTO.class);
        assertEquals(accountDTO.getBalance(), initBalance + amount);
    }

    @Test
    @SneakyThrows
    public void withdrawTest() {
        int initBalance = 300;
        int amount = 200;
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setName(TEST_ACCOUNT_NAME);
        accountDTO.setBalance(initBalance);
        String response = post(APPLICATION_URL + "account", "POST", gson.toJson(accountDTO));
        AccountDTO responseDTO = gson.fromJson(response, AccountDTO.class);
        MoneyChangeDTO moneyChangeDTO = new MoneyChangeDTO();
        moneyChangeDTO.setAccountFrom(responseDTO.getId());
        moneyChangeDTO.setAmount(amount);
        moneyChangeDTO.setOperation(Operation.WITHDRAW);
        post(APPLICATION_URL + "payment", "POST", gson.toJson(moneyChangeDTO));
        response = get(APPLICATION_URL + "account?id=" + responseDTO.getId());
        accountDTO = gson.fromJson(response, AccountDTO.class);
        assertEquals(accountDTO.getBalance(), initBalance - amount);
    }

    @Test
    @SneakyThrows
    public void transferTest() {
        int initBalance1 = 750;
        int initBalance2 = 1300;
        int amount = 200;
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setName(TEST_ACCOUNT_NAME);
        accountDTO.setBalance(initBalance1);
        String response = post(APPLICATION_URL + "account", "POST", gson.toJson(accountDTO));
        AccountDTO response1DTO = gson.fromJson(response, AccountDTO.class);
        accountDTO = new AccountDTO();
        accountDTO.setName(TEST_ACCOUNT_NAME);
        accountDTO.setBalance(initBalance2);
        response = post(APPLICATION_URL + "account", "POST", gson.toJson(accountDTO));
        AccountDTO response2DTO = gson.fromJson(response, AccountDTO.class);
        MoneyChangeDTO moneyChangeDTO = new MoneyChangeDTO();
        moneyChangeDTO.setAccountFrom(response1DTO.getId());
        moneyChangeDTO.setAccountTo(response2DTO.getId());
        moneyChangeDTO.setAmount(amount);
        moneyChangeDTO.setOperation(Operation.TRANSFER);
        post(APPLICATION_URL + "payment", "POST", gson.toJson(moneyChangeDTO));
        response = get(APPLICATION_URL + "account?id=" + response1DTO.getId());
        accountDTO = gson.fromJson(response, AccountDTO.class);
        assertEquals(accountDTO.getBalance(), initBalance1 - amount);
        response = get(APPLICATION_URL + "account?id=" + response2DTO.getId());
        accountDTO = gson.fromJson(response, AccountDTO.class);
        assertEquals(accountDTO.getBalance(), initBalance2 + amount);
    }


    @Test
    @SneakyThrows
    public void withdrawNegativeTest() {
        int initBalance = 300;
        int amount = -200;
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setName(TEST_ACCOUNT_NAME);
        accountDTO.setBalance(initBalance);
        String response = post(APPLICATION_URL + "account", "POST", gson.toJson(accountDTO));
        AccountDTO responseDTO = gson.fromJson(response, AccountDTO.class);
        MoneyChangeDTO moneyChangeDTO = new MoneyChangeDTO();
        moneyChangeDTO.setAccountFrom(responseDTO.getId());
        moneyChangeDTO.setAmount(amount);
        moneyChangeDTO.setOperation(Operation.WITHDRAW);
        boolean ex = false;
        try {
            post(APPLICATION_URL + "payment", "POST", gson.toJson(moneyChangeDTO));
        } catch (IOException e) {
            ex = e.getMessage().contains("Server returned HTTP response code: 400 for URL");
            e.printStackTrace();
        }
        assertTrue(ex);
    }
}
