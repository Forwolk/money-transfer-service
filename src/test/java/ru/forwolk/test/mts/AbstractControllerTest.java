package ru.forwolk.test.mts;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.junit.BeforeClass;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public abstract class AbstractControllerTest {
    Gson gson = new Gson();
    private static boolean serverActive = false;

    @BeforeClass
    @SneakyThrows
    public static void runServer() {
        if (serverActive) {
            return;
        } else {
            serverActive = true;
            Application application = new Application();
            new Thread(application::start).start();
            Thread.sleep(5000);
        }
    }

    protected String post(String stringUrl, String method, String body) throws IOException{
        URL url = new URL(stringUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method);
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        DataOutputStream dos = new DataOutputStream(connection.getOutputStream());
        dos.writeBytes(body);
        dos.flush();
        dos.close();

        printConnectionInfo(connection, body);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        System.out.println("========== RESPONSE ==========" + "\n" +
                response.toString() + "\n" +
                "Code: " + connection.getResponseCode() + "\n" +
                "==============================");

        return response.toString();
    }

    protected String get (String stringUrl) throws IOException{
        URL url = new URL(stringUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        printConnectionInfo(connection, "");

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        System.out.println("========== RESPONSE ==========" + "\n" +
                response.toString() + "\n" +
                "Code: " + connection.getResponseCode() + "\n" +
                "==============================");

        return response.toString();
    }

    @SneakyThrows
    protected void printConnectionInfo(HttpURLConnection connection, String body) {
        String s = "========== REQUEST ===========" + "\n" +
                "URL: " + connection.getURL() + "\n" +
                "Method: " + connection.getRequestMethod() + "\n" +
                "Content type: " + connection.getContentType() + "\n" +
                "User agent: " + connection.getRequestProperty("User-Agent") + "\n" +
                body;
        System.out.println(s);
    }
}
