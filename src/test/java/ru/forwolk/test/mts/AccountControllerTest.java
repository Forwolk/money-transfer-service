package ru.forwolk.test.mts;

import lombok.SneakyThrows;
import org.junit.Test;
import ru.forwolk.test.mts.dto.AccountDTO;

import static org.junit.Assert.assertEquals;
import static ru.forwolk.test.mts.TestConstants.*;

public class AccountControllerTest extends AbstractControllerTest {

    @Test
    @SneakyThrows
    public void createAccountTest() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setName(TEST_ACCOUNT_NAME);
        accountDTO.setBalance(200);
        String response = post(APPLICATION_URL + "account", "POST", gson.toJson(accountDTO));
        AccountDTO responseDTO = gson.fromJson(response, AccountDTO.class);
        assertEquals(responseDTO.getName(), accountDTO.getName());
        assertEquals(responseDTO.getBalance(), accountDTO.getBalance());
    }

    @Test
    @SneakyThrows
    public void updateAccountTest() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setName(TEST_ACCOUNT2_NAME);
        int initBalance = 1250;
        accountDTO.setBalance(initBalance);
        String response = post(APPLICATION_URL + "account", "POST", gson.toJson(accountDTO));
        AccountDTO responseDTO = gson.fromJson(response, AccountDTO.class);
        assertEquals(responseDTO.getBalance(), initBalance);
        accountDTO = responseDTO;
        int finishBalance = 1131;
        accountDTO.setBalance(1131);
        response = post(APPLICATION_URL + "account", "PUT", gson.toJson(accountDTO));
        responseDTO = gson.fromJson(response, AccountDTO.class);
        assertEquals(responseDTO.getBalance(), finishBalance);
        response = get(APPLICATION_URL + "account?id=" + accountDTO.getId());
        responseDTO = gson.fromJson(response, AccountDTO.class);
        assertEquals(responseDTO.getBalance(), finishBalance);
    }

}
