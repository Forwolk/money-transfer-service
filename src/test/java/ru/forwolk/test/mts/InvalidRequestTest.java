package ru.forwolk.test.mts;

import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertTrue;
import static ru.forwolk.test.mts.TestConstants.APPLICATION_URL;

public class InvalidRequestTest extends AbstractControllerTest {

    @Test
    public void invalidRequestTest() {
        assertTrue(validRequest("payment", "}", "POST", 400));
        assertTrue(validRequest("account", "}", "POST", 400));
        assertTrue(validRequest("account", "}", "PUT", 400));
    }

    @Test
    public void invalidPaymentTest() {
        assertTrue(validRequest("payment", "{\"id\":236655959,\"name\":\"test-1\",\"balance\":200}", "POST", 400));
    }

    @Test
    public void invalidAccountUpdateTest() {
        assertTrue(validRequest("account", "{\"id\":236655959,\"name\":\"test-1\",\"balance\":200}", "PUT", 400));
    }

    @Test
    public void invalidAccountCreate1Test() {
        assertTrue(validRequest("account", "{\"balance\":200}", "POST", 400));
    }

    @Test
    public void invalidAccountCreate2Test() {
        assertTrue(validRequest("account", "{\"name\":\"test-1\"}", "POST", 200));
    }

    @Test
    public void invalidAccountCreate3Test() {
        assertTrue(validRequest("account", "{\"id\":265,\"name\":\"test-1\",\"balance\":200}", "POST", 400));
    }

    public boolean validRequest(String path, String body, String method, int errCode) {
        try {
            post(APPLICATION_URL + path, method, body);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return e.getMessage().contains("Server returned HTTP response code: " + errCode + " for URL:");
        }
        return errCode == 200;
    }
}
