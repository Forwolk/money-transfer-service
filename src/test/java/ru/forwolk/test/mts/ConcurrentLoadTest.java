package ru.forwolk.test.mts;

import lombok.SneakyThrows;
import org.junit.Test;
import ru.forwolk.test.mts.dto.AccountDTO;
import ru.forwolk.test.mts.dto.MoneyChangeDTO;
import ru.forwolk.test.mts.dto.Operation;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static ru.forwolk.test.mts.TestConstants.APPLICATION_URL;
import static ru.forwolk.test.mts.TestConstants.TEST_ACCOUNT2_NAME;
import static ru.forwolk.test.mts.TestConstants.TEST_ACCOUNT_NAME;

public class ConcurrentLoadTest extends AbstractControllerTest {
    private final int THREAD_COUNT = 8;
    private final int OPERATION_COUNT = 100;

    @Test
    @SneakyThrows
    public void depositLoadTest() {
        int initBalance = 0;
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setName(TEST_ACCOUNT_NAME);
        accountDTO.setBalance(initBalance);
        String response = post(APPLICATION_URL + "account", "POST", gson.toJson(accountDTO));
        AccountDTO responseDTO = gson.fromJson(response, AccountDTO.class);

        final AtomicInteger balance = new AtomicInteger(initBalance);

        Thread[] threads = new Thread[THREAD_COUNT];

        for (int i = 0; i < THREAD_COUNT; i ++) {
            threads[i] = new Thread(() -> {
                MoneyChangeDTO moneyChangeDTO = new MoneyChangeDTO();
                moneyChangeDTO.setAccountTo(responseDTO.getId());
                moneyChangeDTO.setOperation(Operation.DEPOSIT);
                for (int k = 0; k < OPERATION_COUNT; k ++) {
                    int amount = (int) (Math.random() * 1000);
                    synchronized (balance) {
                        balance.addAndGet(amount);
                    }
                    moneyChangeDTO.setAmount(amount);
                    try {
                        post(APPLICATION_URL + "payment", "POST", gson.toJson(moneyChangeDTO));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        long start;
        long finish;
        start = System.nanoTime();
        Arrays.stream(threads).forEach(Thread::start);

        for (Thread thread : threads) {
            thread.join();
        }
        finish = System.nanoTime();


        response = get(APPLICATION_URL + "account?id=" + responseDTO.getId());
        accountDTO = gson.fromJson(response, AccountDTO.class);
        assertEquals(balance.get(), accountDTO.getBalance());

        System.out.printf("Time: %.3f s\nOperation time: %.3f ms", (finish - start) / 1_000_000_000f, (finish - start) / (1_000_000.0f * THREAD_COUNT * OPERATION_COUNT));
    }

    @Test
    @SneakyThrows
    public void withdrawLoadTest() {
        int initBalance = Integer.MAX_VALUE;
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setName(TEST_ACCOUNT_NAME);
        accountDTO.setBalance(initBalance);
        String response = post(APPLICATION_URL + "account", "POST", gson.toJson(accountDTO));
        AccountDTO responseDTO = gson.fromJson(response, AccountDTO.class);

        final AtomicInteger balance = new AtomicInteger(initBalance);

        Thread[] threads = new Thread[THREAD_COUNT];

        for (int i = 0; i < THREAD_COUNT; i ++) {
            threads[i] = new Thread(() -> {
                MoneyChangeDTO moneyChangeDTO = new MoneyChangeDTO();
                moneyChangeDTO.setAccountFrom(responseDTO.getId());
                moneyChangeDTO.setOperation(Operation.WITHDRAW);
                for (int k = 0; k < OPERATION_COUNT; k ++) {
                    int amount = (int) (Math.random() * 1000);
                    synchronized (balance) {
                        balance.addAndGet(-amount);
                    }
                    moneyChangeDTO.setAmount(amount);
                    try {
                        post(APPLICATION_URL + "payment", "POST", gson.toJson(moneyChangeDTO));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        long start;
        long finish;
        start = System.nanoTime();
        Arrays.stream(threads).forEach(Thread::start);

        for (Thread thread : threads) {
            thread.join();
        }
        finish = System.nanoTime();


        response = get(APPLICATION_URL + "account?id=" + responseDTO.getId());
        accountDTO = gson.fromJson(response, AccountDTO.class);
        assertEquals(balance.get(), accountDTO.getBalance());

        System.out.printf("Time: %.3f s\nOperation time: %.3f ms", (finish - start) / 1_000_000_000f, (finish - start) / (1_000_000.0f * THREAD_COUNT * OPERATION_COUNT));
    }
    @Test
    @SneakyThrows
    public void transferLoadTest() {
        int initBalance1 = Integer.MAX_VALUE;
        int initBalance2 = 0;
        AccountDTO account1DTO = new AccountDTO();
        account1DTO.setName(TEST_ACCOUNT_NAME);
        account1DTO.setBalance(initBalance1);
        String response = post(APPLICATION_URL + "account", "POST", gson.toJson(account1DTO));
        AccountDTO responseDTO = gson.fromJson(response, AccountDTO.class);
        account1DTO.setId(responseDTO.getId());
        AccountDTO account2DTO = new AccountDTO();
        account2DTO.setName(TEST_ACCOUNT2_NAME);
        account2DTO.setBalance(initBalance2);
        response = post(APPLICATION_URL + "account", "POST", gson.toJson(account2DTO));
        responseDTO = gson.fromJson(response, AccountDTO.class);
        account2DTO.setId(responseDTO.getId());

        final AtomicInteger balance1 = new AtomicInteger(initBalance1);
        final AtomicInteger balance2 = new AtomicInteger(initBalance2);

        Thread[] threads = new Thread[THREAD_COUNT];

        int acc1 = account1DTO.getId();
        int acc2 = account2DTO.getId();

        for (int i = 0; i < THREAD_COUNT; i ++) {
            threads[i] = new Thread(() -> {
                MoneyChangeDTO moneyChangeDTO = new MoneyChangeDTO();
                moneyChangeDTO.setAccountFrom(acc1);
                moneyChangeDTO.setAccountTo(acc2);
                moneyChangeDTO.setOperation(Operation.TRANSFER);
                for (int k = 0; k < OPERATION_COUNT; k ++) {
                    int amount = (int) (Math.random() * 1000);
                    synchronized (balance1) {
                        balance1.addAndGet(-amount);
                    }
                    synchronized (balance2) {
                        balance2.addAndGet(amount);
                    }
                    moneyChangeDTO.setAmount(amount);
                    try {
                        post(APPLICATION_URL + "payment", "POST", gson.toJson(moneyChangeDTO));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        long start;
        long finish;
        start = System.nanoTime();
        Arrays.stream(threads).forEach(Thread::start);

        for (Thread thread : threads) {
            thread.join();
        }
        finish = System.nanoTime();


        response = get(APPLICATION_URL + "account?id=" + account1DTO.getId());
        account1DTO = gson.fromJson(response, AccountDTO.class);
        assertEquals(balance1.get(), account1DTO.getBalance());
        response = get(APPLICATION_URL + "account?id=" + account2DTO.getId());
        account2DTO = gson.fromJson(response, AccountDTO.class);
        assertEquals(balance2.get(), account2DTO.getBalance());

        assertEquals(initBalance1 + initBalance2, account1DTO.getBalance() + account2DTO.getBalance());

        System.out.printf("Time: %.3f s\nOperation time: %.3f ms", (finish - start) / 1_000_000_000f, (finish - start) / (1_000_000.0f * THREAD_COUNT * OPERATION_COUNT));
    }

}
