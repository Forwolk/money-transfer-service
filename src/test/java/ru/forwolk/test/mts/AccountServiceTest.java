package ru.forwolk.test.mts;

import org.junit.Test;
import ru.forwolk.test.mts.entity.Account;
import ru.forwolk.test.mts.service.AccountService;
import ru.forwolk.test.mts.service.SingletonManager;

import static org.junit.Assert.*;
import static ru.forwolk.test.mts.TestConstants.TEST_ACCOUNT_NAME;

public class AccountServiceTest extends AbstractServiceTest {

    @Test
    public void accountCreationTest() {
        AccountService accountService = SingletonManager.getInstance().get(AccountService.class);
        Account account = accountService.create(TEST_ACCOUNT_NAME, 0);
        Account account2 = accountService.getAccountById(account.getId()).orElseGet(null);
        assertEquals(account, account2);
    }

    @Test
    public void accountUpdateTest() {
        AccountService accountService = SingletonManager.getInstance().get(AccountService.class);
        Account account = accountService.create(TEST_ACCOUNT_NAME, 0);
        int balance = (int) (1000000 * Math.random());
        account.setBalance(balance);
        accountService.update(account);
        Account account2 = accountService.getAccountById(account.getId()).get();
        assertEquals(account.getBalance(), account2.getBalance());
    }

    @Test
    public void accountDeleteTest() {
        AccountService accountService = SingletonManager.getInstance().get(AccountService.class);
        Account account = accountService.create(TEST_ACCOUNT_NAME, 0);
        assertTrue(accountService.getAccountById(account.getId()).isPresent());
        accountService.delete(account);
        assertFalse(accountService.getAccountById(account.getId()).isPresent());
    }
}
